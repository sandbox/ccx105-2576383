This project was forked off of the field_weight module.

It allows users to manage the weight of field groups on specific nodes.

Installation:

Download & enable module
Configure the module -> choose the content types where group 
weight modifications are desired on a per node basis

Visit a node for the enabled content type
Modify the group weights by clicking on the "Field group display weights" tab

Known limitation:
Currently the weights are only modifiable for the default view mode.
