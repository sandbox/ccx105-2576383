<?php

/**
 * @file
 * Field group weight admin settings form.
 */

/**
 * Implements hook_settings_form().
 */
function field_group_weight_settings_form() {
  $form = array();

  $node_types = array_map('check_plain', node_type_get_names());

  $form['field_group_weight']['node_types']['field_group_weight_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enabled content types'),
    '#description' => t('Content (node) types that field group display weights can be used on. This will add an additional tab to nodes.'),
    '#options' => $node_types,
    '#default_value' => variable_get('field_group_weight_node_types', array()),
    '#multiple' => TRUE,
  );

  return system_settings_form($form);
}
